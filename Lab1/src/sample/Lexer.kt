package sample

import java.util.ArrayList
import java.util.HashMap

class Lexer(private val input: String) {
    private val length: Int = input.length

    private val tokens: MutableList<Token>

    private var pos: Int = 0

    init {

        tokens = ArrayList()
    }

    fun tokenize(): List<Token> {
        while (pos < length) {
            val current = peek(0)
            when {
                Character.isDigit(current) -> tokenizeNumber()
                Character.isLetter(current) -> tokenizeWord()
                current == '"' -> tokenizeText()
                OPERATOR_CHARS.indexOf(current.toInt()) != -1 -> tokenizeOperator()
                else -> // whitespaces
                    next()
            }
        }
        return tokens
    }

    private fun tokenizeNumber() {
        val buffer = StringBuilder()
        var current = peek(0)
        while (true) {
            if (current == '.') {
                if (buffer.indexOf(".") != -1) throw RuntimeException("Invalid float number")
            } else if (!Character.isDigit(current)) {
                break
            }
            buffer.append(current)
            current = next()
        }
        addToken(TokenType.NUMBER, buffer.toString())
    }

    private fun tokenizeOperator() {
        var current = peek(0)
        if (current == '/') {
            if (peek(1) == '/') {
                next()
                next()
                tokenizeComment()
                return
            } else if (peek(1) == '*') {
                next()
                next()
                tokenizeMultilineComment()
                return
            }
        }
        val buffer = StringBuilder()
        while (true) {
            val text = buffer.toString()
            if (!OPERATORS.containsKey(text + current) && text.isNotEmpty()) {
                OPERATORS[text]?.let { addToken(it) }
                return
            }
            buffer.append(current)
            current = next()
        }
    }

    private fun tokenizeWord() {
        val buffer = StringBuilder()
        var current = peek(0)
        while (true) {
            if (!Character.isLetterOrDigit(current) && current != '_' && current != '$') {
                break
            }
            buffer.append(current)
            current = next()
        }

        val word = buffer.toString()
        when (word) {
            "print" -> addToken(TokenType.PRINT)
            "if" -> addToken(TokenType.IF)
            "else" -> addToken(TokenType.ELSE)
            "var" -> addToken(TokenType.VAR)
            "goto" -> addToken(TokenType.GOTOURL)
            "clickByClassName" -> addToken(TokenType.CLICKBYCLASS)
            "clickById" -> addToken(TokenType.CLICKBYID)
            "fillInputById" -> addToken(TokenType.FILLINPUT)
            "pause" -> addToken(TokenType.PAUSE)
            "while" -> addToken(TokenType.WHILE)
            else -> addToken(TokenType.WORD, word)
        }
    }

    private fun tokenizeText() {
        next()// skip "
        val buffer = StringBuilder()
        var current = peek(0)
        while (true) {
            if (current == '\\') {
                current = next()
                when (current) {
                    '"' -> {
                        current = next()
                        buffer.append('"')
                    }
                    'n' -> {
                        current = next()
                        buffer.append('\n')
                    }
                    't' -> {
                        current = next()
                        buffer.append('\t')
                    }
                }
                buffer.append('\\')
                continue
            }
            if (current == '"') break
            buffer.append(current)
            current = next()
        }
        next() // skip closing "

        addToken(TokenType.TEXT, buffer.toString())
    }

    private fun tokenizeComment() {
        var current = peek(0)
        while ("\r\n\u0000".indexOf(current.toInt()) == -1) {
            current = next()
        }
    }

    private fun tokenizeMultilineComment() {
        var current = peek(0)
        while (true) {
            if (current == '\u0000') throw RuntimeException("Missing close tag")
            if (current == '*' && peek(1) == '/') break
            current = next()
        }
        next() // *
        next() // /
    }

    private operator fun next(): Char {
        pos++
        return peek(0)
    }

    private fun peek(relativePosition: Int): Char {
        val position = pos + relativePosition
        return if (position >= length) '\u0000' else input[position]
    }

    private fun addToken(type: TokenType, text: String = "") {
        tokens.add(Token(type, text))
    }

    companion object {

        private val OPERATOR_CHARS = "+-*/{}()=<>!&|"

        private val OPERATORS: MutableMap<String, TokenType>

        init {
            OPERATORS = HashMap()
            OPERATORS["var"] = TokenType.VAR
            OPERATORS["goto"] = TokenType.GOTOURL
            OPERATORS["clickByClassName"] = TokenType.CLICKBYCLASS
            OPERATORS["clickById"] = TokenType.CLICKBYID
            OPERATORS["fillInputById"] = TokenType.FILLINPUT
            OPERATORS["pause"] = TokenType.PAUSE
            OPERATORS["if"] = TokenType.IF
            OPERATORS["else"] = TokenType.ELSE
            OPERATORS["{"] = TokenType.LBRACE
            OPERATORS["}"] = TokenType.RBRACE
            OPERATORS["+"] = TokenType.PLUS
            OPERATORS["-"] = TokenType.MINUS
            OPERATORS["*"] = TokenType.STAR
            OPERATORS["/"] = TokenType.SLASH
            OPERATORS["("] = TokenType.LPAREN
            OPERATORS[")"] = TokenType.RPAREN
            OPERATORS["="] = TokenType.EQ
            OPERATORS["<"] = TokenType.LT
            OPERATORS[">"] = TokenType.GT

            OPERATORS["!"] = TokenType.EXCL
            OPERATORS["&"] = TokenType.AMP
            OPERATORS["|"] = TokenType.BAR

            OPERATORS["=="] = TokenType.EQEQ
            OPERATORS["!="] = TokenType.EXCLEQ
            OPERATORS["<="] = TokenType.LTEQ
            OPERATORS[">="] = TokenType.GTEQ

            OPERATORS["&&"] = TokenType.AMPAMP
            OPERATORS["||"] = TokenType.BARBAR
        }

        private fun isHexNumber(current: Char): Boolean {
            return "abcdef".indexOf(Character.toLowerCase(current).toInt()) != -1
        }
    }
}

private fun String.indexOf(toInt: Int): Any {
    return this.indexOf(toInt.toChar())
}
