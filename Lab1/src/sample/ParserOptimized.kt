package sample

import sample.common.Utils
import sample.expressions.*
import sample.expressions.binary.*
import sample.expressions.binary.compare.*
import sample.expressions.binary.math.DivisionExpression
import sample.expressions.binary.math.MinusExpression
import sample.expressions.binary.math.MultiplyExpression
import sample.expressions.binary.math.PlusExpression
import sample.expressions.unary.*

class ParserOptimized(private val tokens: List<Token>, private val context: Context) {
    private val size: Int = tokens.size

    private var pos: Int = 0

    fun parse(): ExpressionBase {
        val result = StatementsExpression()
        while (!match(TokenType.EOF)) {
            result.addStatement(statement())
        }
        return result
    }

    private fun block(): ExpressionBase {
        val block = StatementsExpression()
        consume(TokenType.LBRACE)
        while (!match(TokenType.RBRACE)) {
            block.addStatement(statement())
        }
        return block
    }

    private fun statementOrBlock(): ExpressionBase? {
        return if (get().type == TokenType.LBRACE) block() else statement()
    }

    private fun statement(): ExpressionBase? {
        if (match(TokenType.GOTOURL))
            return expression()?.let { GoToUrlExpression(it) }
        if (match(TokenType.PAUSE))
            return expression()?.let { PauseExpression(it) }
        if (match(TokenType.CLICKBYCLASS)) {
            return expression()?.let { ClickByClassNameExpression(it) }
        }
        if (match(TokenType.CLICKBYID)) {
            return expression()?.let { ClickBtnByIdExpression(it) }
        }
        if (match(TokenType.FILLINPUT)) {
            return expression()?.let { expression()?.let { it1 -> FillInputExpression(it, it1) } }
        }
        if (match(TokenType.PRINT)) {
            return expression()?.let { PrintExpression(it) }
        }
        if (match(TokenType.IF)) {
            return ifElse()
        }
        return if (match(TokenType.WHILE)) {
            whileStatement()
        } else assignmentStatement()


        //        if (match(TokenType.BREAK)) {
        //            return new BreakStatement();
        //        }

    }

    private fun assignmentStatement(): ExpressionBase? {
        // WORD EQ
        val current = get()
        if (match(TokenType.WORD) && get().type == TokenType.EQ) {
            val variable = current.text
            consume(TokenType.EQ)
            return variable?.let { VariableExpression(it) }?.let { expression()?.let { it1 -> AssignmentExpression(it, it1) } }
        }
        throw RuntimeException("Unknown statement")
    }

    private fun ifElse(): ExpressionBase? {
        var condition : ExpressionBase? = expression()
        condition = Utils.optimizeConditional(condition, context)
        val ifStatement = statementOrBlock() ?: return null
        val elseStatement: ExpressionBase?
        if (match(TokenType.ELSE)) {
            elseStatement = statementOrBlock()
        } else {
            elseStatement = null
        }

        if(condition is LogicExpression) {
            println("Logical exp")
            if(condition is TrueExpression)
            {
                return ifStatement
            } else if(condition is FalseExpression) {
                return elseStatement
            }
        }
        println("Full exp")

        return if(elseStatement == null)
        {
            IfElseExpression(IfExpression(condition!!, ifStatement), null)
        } else {
            IfElseExpression(IfExpression(condition!!, ifStatement), elseStatement)
        }    }

    private fun whileStatement(): ExpressionBase? {
        var condition : ExpressionBase? = expression()
        condition = Utils.optimizeConditional(condition, context)
        val statement = statementOrBlock() ?: return null

        if(condition is TrueExpression)
        {
            return statement
        } else if(condition is FalseExpression) {
            return null
        }

        return condition?.let { WhileExpression(it, statement) }
    }

    private fun expression(): ExpressionBase? {
        return logicalOr()
    }

    private fun logicalOr(): ExpressionBase? {
        var result = logicalAnd()

        while (true) {
            if (match(TokenType.BARBAR)) {
                result = result?.let { logicalAnd()?.let { it1 -> OrExpression(it, it1) } }
                continue
            }
            break
        }

        return result
    }

    private fun logicalAnd(): ExpressionBase? {
        var result = equality()

        while (true) {
            if (match(TokenType.AMPAMP)) {
                result = result?.let { equality()?.let { it1 -> AndExpression(it, it1) } }
                continue
            }
            break
        }

        return result
    }

    private fun equality(): ExpressionBase? {
        val result = conditional()

        if (match(TokenType.EQEQ)) {
            return result?.let { conditional()?.let { it1 -> EqualsExpression(it, it1) } }
        }
        return if (match(TokenType.EXCLEQ)) {
            result?.let { conditional()?.let { it1 -> NotEqualsExpression(it, it1) } }
        } else result

    }

    private fun conditional(): ExpressionBase? {
        var result = additive()

        while (true) {
            if (match(TokenType.LT)) {
                result = additive()?.let { result?.let { it1 -> LessExpression(it1, it) } }
                continue
            }
            if (match(TokenType.LTEQ)) {
                result = result?.let { additive()?.let { it1 -> LessEqualsExpression(it, it1) } }
                continue
            }
            if (match(TokenType.GT)) {
                result = result?.let { additive()?.let { it1 -> GreatExpression(it, it1) } }
                continue
            }
            if (match(TokenType.GTEQ)) {
                result = result?.let { additive()?.let { it1 -> GreatEqualsExpression(it, it1) } }
                continue
            }
            break
        }

        return result
    }

    private fun additive(): ExpressionBase? {
        var result = multiplicative()

        while (true) {
            if (match(TokenType.PLUS)) {
                result = multiplicative()?.let { result?.let { it1 -> PlusExpression(it1, it) } }
                continue
            }
            if (match(TokenType.MINUS)) {
                result = multiplicative()?.let { result?.let { it1 -> MinusExpression(it1, it) } }
                continue
            }
            break
        }

        return result
    }

    private fun multiplicative(): ExpressionBase? {
        var result = unary()

        while (true) {
            // 2 * 6 / 3
            if (match(TokenType.STAR)) {
                result = result?.let { unary()?.let { it1 -> MultiplyExpression(it, it1) } }
                continue
            }
            if (match(TokenType.SLASH)) {
                result = result?.let { unary()?.let { it1 -> DivisionExpression(it, it1) } }
                continue
            }
            break
        }

        return result
    }

    private fun unary(): ExpressionBase? {
        if (match(TokenType.MINUS)) {
            val exp = primary()
            if (exp is ConstantExpression) {
                val opt = exp.solve(null)
                if (opt.isPresent)
                    return UnaryMinusExpression(opt.get().toString())
            }
        }
        return if (match(TokenType.PLUS)) {
            primary()
        } else primary()
    }

    private fun primary(): ExpressionBase? {
        val current = get()
        if (match(TokenType.NUMBER)) {
            return current.text?.let { ConstantExpression(it) }
        }
        if (match(TokenType.HEX_NUMBER)) {
            return ConstantExpression(java.lang.Long.parseLong(current.text, 16).toString())
        }
        if (match(TokenType.WORD)) {
            return current.text?.let { VariableExpression(it) }
        }
        if (match(TokenType.TEXT)) {
            return current.text?.let { ConstantExpression(it) }
        }
        if (match(TokenType.LPAREN)) {
            val result = expression()
            match(TokenType.RPAREN)
            return result
        }
        throw RuntimeException("Unknown expression")
    }

    private fun consume(type: TokenType) {
        val current = get()
        if (type != current.type) throw RuntimeException("Token $current doesn't match $type")
        pos++
    }

    private fun match(type: TokenType): Boolean {
        val current = get()
        if (type != current.type) return false
        pos++
        return true
    }

    private fun get(): Token {
        val position = pos
        return if (position >= size) EOF else tokens[position]
    }

    companion object {

        private val EOF = Token(TokenType.EOF, "")
    }
}