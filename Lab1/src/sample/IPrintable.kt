package sample

interface IPrintable {
    fun print(text: String)
    fun println(text: String)
}
