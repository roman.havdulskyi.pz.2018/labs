package sample

import sample.common.Fault
import sample.common.TaskCallback
import sample.expressions.ExpressionBase

object InterpreterV2 {
    fun ExecuteAsync(text: String, context: Context, resultCallback: TaskCallback<ArrayList<ExpressionBase?>?>) {
        Thread {
            try {
                val lexer = Lexer(text)
                val tokens = lexer.tokenize()
                val parser = Parser(tokens)
                val base = parser.parse()
                val parserOptimized = ParserOptimized(tokens, context)
                val baseOptimized = parserOptimized.parse()
                val result = arrayListOf<ExpressionBase?>(base, baseOptimized)
                resultCallback.onCompleted(result, null)
                base.solve(context)
                println("done")
            } catch (e: Exception) {
                resultCallback.onCompleted(null, Fault(e))
            }
        }.start()
    }
}
