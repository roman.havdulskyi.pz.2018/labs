//package sample;
//
//import sample.common.Utils;
//import sample.expressions.*;
//import sample.expressions.binary.*;
//import sample.expressions.binary.compare.*;
//import sample.expressions.binary.math.DivisionExpression;
//import sample.expressions.binary.math.MinusExpression;
//import sample.expressions.binary.math.MultiplyExpression;
//import sample.expressions.binary.math.PlusExpression;
//import sample.expressions.unary.*;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.List;
//
//public class Interpreter {
//    private ExpressionBase Parse(String firstToken) throws Exception {
//        if (firstToken.length() == 0)
//            return null;
//        ExpressionBase expression = null;
//
//        if (firstToken.equals("var")) {
//            expression = new VarExpression();
//        } else if (firstToken.equals("=")) {
//            expression = new AssignmentExpression();
//        } else if (firstToken.equals("+")) {
//            expression = new PlusExpression();
//        } else if (firstToken.equals("-")) {
//            expression = new MinusExpression();
//        } else if (firstToken.equals("*")) {
//            expression = new MultiplyExpression();
//        } else if (firstToken.equals("/")) {
//            expression = new DivisionExpression();
//        } else if (firstToken.equals("while")) {
//            expression = new WhileExpression();
//        } else if (firstToken.equals(">=")) {
//            expression = new GreatEqualsExpression();
//        } else if (firstToken.equals(">")) {
//            expression = new GreatExpression();
//        } else if (firstToken.equals("<=")) {
//            expression = new LessEqualsExpression();
//        } else if (firstToken.equals("<")) {
//            expression = new LessExpression();
//        } else if (firstToken.equals("==")) {
//            expression = new EqualsExpression();
//        } else if (firstToken.equals("goto")) {
//            expression = new GoToUrlExpression();
//        } else if (firstToken.equals("clickById")) {
//            expression = new ClickBtnByIdExpression();
//        } else if (firstToken.equals("clickByClassName")) {
//            expression = new ClickByClassNameExpression();
//        } else if (firstToken.equals("fillInputById")) {
//            expression = new FillInputExpression();
//        } else if (firstToken.equals("pause")) {
//            expression = new PauseExpression();
//        } else if (firstToken.equals("print")) {
//            expression = new PrintExpression();
//        } else if (Utils.isConstExp(firstToken)) {
//            expression = new ConstantExpression(firstToken.replaceAll("\"", ""));
//        } else if (Utils.isNumeric(firstToken)) {
//            expression = new ConstantExpression(firstToken);
//        } else if (Utils.isLetterOrDigit(firstToken)) {
//            expression = new VariableExpression(firstToken);
//        } else {
//            throw new Exception("Unknow expression " + firstToken);
//        }
//
//        return expression;
//    }
//
//    private ExpressionBase genStatementExp(List<String> list) {
//        list.remove("{");
//        list.remove("}");
//        if (list.isEmpty())
//            return null;
//        List<ExpressionBase> statementTokens = new ArrayList<>();
//        String[] statementArray = list.toArray(new String[0]);
//        int index = 0;
//        do {
//            ExpressionBase token = null;
//            try {
//                token = Parse(statementArray[index]);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            if (token != null)
//                statementTokens.add(token);
//            index++;
//        } while (index < statementArray.length);
//        return CreateTree(statementTokens);
//    }
//
//    private IfExpression genIfExp(List<String> condContainer, StatementsExpression expression, Context context) throws Exception {
//        ExpressionBase ifToken = new IfExpression();
//        condContainer.removeAll(Collections.singleton("("));
//        condContainer.removeAll(Collections.singleton(")"));
//        condContainer.removeAll(Collections.singleton("{"));
//        condContainer.removeAll(Collections.singleton("}"));
//        condContainer.removeAll(Collections.singleton("else"));
//        condContainer.removeAll(Collections.singleton("if"));
//
//        if (!condContainer.isEmpty()) {
//            String[] condArray = condContainer.toArray(new String[0]);
//            List<ExpressionBase> condTokens = new ArrayList<>();
//            int index = 0;
//            do {
//                ExpressionBase token = Parse(condArray[index]);
//                if (token != null)
//                    condTokens.add(token);
//                index++;
//            } while (index < condArray.length);
//            var condTree = CreateTree(condTokens);
//
//            ifToken = AddToTree(ifToken, condTree);
//            condContainer.clear();
//
//        }
//        ifToken = AddToTree(ifToken, expression);
//        if (ifToken instanceof IfExpression) {
//            return (IfExpression) ifToken;
//        } else {
//            return null;
//        }
//    }
//
//    private void executeIf() {
//
//    }
//
//    private void executeWhile(List<String> condContainer, StatementsExpression statementsExpression, Context context) throws Exception {
//        condContainer.remove("(");
//        condContainer.remove(")");
//        condContainer.remove("{");
//        condContainer.remove("while");
//
//        String[] condArray = condContainer.toArray(new String[0]);
//        List<ExpressionBase> condTokens = new ArrayList<>();
//        int index = 0;
//        do {
//            ExpressionBase token = Parse(condArray[index]);
//            if (token != null)
//                condTokens.add(token);
//            index++;
//        } while (index < condArray.length);
//        var condTree = CreateTree(condTokens);
//
//        ExpressionBase whileToken = Parse("while");
//        whileToken = AddToTree(whileToken, condTree);
//        whileToken = AddToTree(whileToken, statementsExpression);
//        whileToken.solve(context);
//    }
//
//    public void ExecuteAsync(String text, Context context) {
//        new Thread(() -> {
//            try {
//                Execute(text, context);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }).start();
//    }
//
//    public void Execute(String text, Context context) throws Exception {
//        String[] lines = text.split("\n");
//        boolean foundWhile = false;
//        boolean fillCond = true;
//        boolean fillStat = false;
//        boolean foundIfBody = false;
//        List<String> condContainer = new ArrayList<>();
//        ExpressionBase ifElseExpression = new IfElseExpression();
//        StatementsExpression statementsExpression = new StatementsExpression();
//
//        for (String line : lines) {
//            List<String> statementsContainer = new ArrayList<>();
//            List<ExpressionBase> tokens = new ArrayList<>();
//
//            line = line.replaceAll("\r", "");
//            String expression = line.trim();
//
//            String[] tokenArray = expression.split(" ");
//
//            if(tokenArray.length == 0 || expression.length() == 0 || Utils.isNullOrWhitespace(expression))
//                continue;
//
//            boolean fillIf = tokenArray[0].equals("if") || tokenArray.length > 1 && tokenArray[1].equals("else") || foundIfBody;
//
//            if (fillIf) {
//                foundIfBody = true;
//                List<String> temp = Arrays.asList(tokenArray);
//                if (temp.contains("}")) {
//                    fillCond = true;
//                    fillStat = false;
//                    if (temp.contains("{")) {
//                        for (String token : temp) {
//                            if(token.equals("}"))
//                            {
//                                if (!statementsContainer.isEmpty())
//                                    statementsExpression.addStatement(genStatementExp(statementsContainer));
//
//                                if (statementsExpression.statementsSize() > 0) {
//                                    ExpressionBase ifElseExpTemp = new IfElseExpression();
//                                    ExpressionBase exp = genIfExp(condContainer, statementsExpression, context);
//                                    ifElseExpTemp = AddToTree(ifElseExpTemp, exp);
//                                    if (exp != null)
//                                        ifElseExpression = AddToTree(ifElseExpression, ifElseExpTemp);
//                                }
//                                statementsExpression = new StatementsExpression();
//                                if (!temp.contains("else")) {
//                                    foundIfBody = false;
//                                    fillCond = true;
//                                    fillStat = false;
//                                    ifElseExpression.solve(context);
//                                    ifElseExpression = new IfElseExpression();
//                                }
//                            } else if (token.equals("{")) {
//                                fillCond = false;
//                                fillStat = true;
//                            }
//                            if (fillCond)
//                                condContainer.add(token);
//                            else
//                                statementsContainer.add(token);
//                        }
//
//                        if (fillStat && !statementsContainer.isEmpty())
//                            statementsExpression.addStatement(genStatementExp(statementsContainer));
//
//                    } else {
//                        condContainer.addAll(temp);
//                    }
//
//                    if (!statementsContainer.isEmpty())
//                        statementsExpression.addStatement(genStatementExp(statementsContainer));
//
//                    if (statementsExpression.statementsSize() > 0) {
//                        ExpressionBase exp = genIfExp(condContainer, statementsExpression, context);
//                        if (exp != null)
//                            ifElseExpression = AddToTree(ifElseExpression, exp);
//                    }
//                    statementsExpression = new StatementsExpression();
//                    if (!temp.contains("else")) {
//                        foundIfBody = false;
//                        fillCond = true;
//                        fillStat = false;
//                        ifElseExpression.solve(context);
//                        ifElseExpression = new IfElseExpression();
//                    }
//                    //execute
//                } else if (temp.contains("{")) {
//                    if (fillCond) {
//                        for (String token : temp) {
//                            if (token.equals("{")) {
//                                fillCond = false;
//                                fillStat = true;
//                            }
//                            if (fillCond)
//                                condContainer.add(token);
//                            else
//                                statementsContainer.add(token);
//                        }
//                    }
//
//                    if (fillStat && !statementsContainer.isEmpty())
//                        statementsExpression.addStatement(genStatementExp(statementsContainer));
//
//                } else {
//                    if (fillStat)
//                        statementsExpression.addStatement(genStatementExp(temp));
//                    else
//                        condContainer.addAll(temp);
//                }
//            } else if (tokenArray[0].equals("while") || foundWhile) {
//                foundWhile = true;
//                List<String> temp = Arrays.asList(tokenArray);
//                if (temp.contains("}")) {
//                    if (temp.contains("{")) {
//                        if (fillCond) {
//                            for (String token : temp) {
//                                if (token.equals("{")) {
//                                    fillCond = false;
//                                    fillStat = true;
//                                }
//                                if (fillCond)
//                                    condContainer.add(token);
//                                else
//                                    statementsContainer.add(token);
//                            }
//
//                            if (fillStat && !statementsContainer.isEmpty())
//                                statementsExpression.addStatement(genStatementExp(statementsContainer));
//
//                        }
//                    } else {
//                        if (fillStat)
//                            statementsContainer.addAll(temp);
//                        else
//                            condContainer.addAll(temp);
//                    }
//                    if (!statementsContainer.isEmpty())
//                        statementsExpression.addStatement(genStatementExp(statementsContainer));
//
//                    executeWhile(condContainer, statementsExpression, context);
//                    statementsExpression = new StatementsExpression();
//                    foundWhile = false;
//                    fillCond = true;
//                    fillStat = false;
//                    //execute
//                } else if (temp.contains("{")) {
//                    if (fillCond) {
//                        for (String token : temp) {
//                            if (token.equals("{")) {
//                                fillCond = false;
//                                fillStat = true;
//                            }
//                            if (fillCond)
//                                condContainer.add(token);
//                            else
//                                statementsContainer.add(token);
//                        }
//                    }
//
//                    if (fillStat && !statementsContainer.isEmpty())
//                        statementsExpression.addStatement(genStatementExp(statementsContainer));
//
//                } else {
//                    if (fillStat)
//                        statementsExpression.addStatement(genStatementExp(temp));
//                    else
//                        condContainer.addAll(temp);
//                }
//            } else {
//
//                int index = 0;
//                do {
//                    ExpressionBase token = Parse(tokenArray[index]);
//                    if (token != null)
//                        tokens.add(token);
//                    index++;
//                }
//                while (index < tokenArray.length);
//
//
//                var solver = CreateTree(tokens);
//
//                solver.solve(context);
//            }
//        }
//    }
//
//
//    private ExpressionBase CreateTree(List<ExpressionBase> expressions) {
//        ExpressionBase tree = null;
//
//        for (ExpressionBase token : expressions) {
//            tree = AddToTree(tree, token);
//        }
//
//        return tree;
//    }
//
//    private ExpressionBase CheckTypeAndAdd(ExpressionBase tree, ExpressionBase node) {
//
//        if (tree instanceof BinaryExpression) {
//            BinaryExpression ex2 = (BinaryExpression) tree;
//            if (ex2.getOperand1() == null) {
//                ex2.setOperand1(AddToTree(ex2.getOperand2(), node));
//            } else {
//                ex2.setOperand2(AddToTree(ex2.getOperand2(), node));
//            }
//            return ex2;
//        } else if (tree instanceof UnaryExpression) {
//            UnaryExpression ex1 = (UnaryExpression) tree;
//            ex1.setOperand1(AddToTree(ex1.getOperand1(), node));
//            return ex1;
//        } else {
//            return AddToTree(node, tree);
//        }
//    }
//
//    private ExpressionBase AddToTree(ExpressionBase tree, ExpressionBase node) {
//        if (tree == null) {
//            return node;
//        }
//
//        if (tree.getPriority() > node.getPriority() || node instanceof IfExpression) {
//            return CheckTypeAndAdd(tree, node);
//        } else if (tree.getPriority() == node.getPriority()) {
//            return CheckTypeAndAdd(node, tree);
//        } else {
//            return AddToTree(node, tree);
//        }
//
//    }
//}
