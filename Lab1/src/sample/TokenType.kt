package sample

enum class TokenType {

    NUMBER,
    HEX_NUMBER,
    WORD,
    TEXT,

    // keyword
    WHILE,
    VAR,
    GOTOURL,
    CLICKBYID,
    CLICKBYCLASS,
    FILLINPUT,

    PAUSE,
    BREAK,


    PRINT,
    IF,
    ELSE,

    PLUS,
    MINUS,
    STAR,
    SLASH,
    EQ,
    EQEQ,
    EXCL,
    EXCLEQ,
    LT,
    LTEQ,
    GT,
    GTEQ,

    BAR,
    BARBAR,
    AMP,
    AMPAMP,

    LBRACE, // {
    RBRACE, // }
    LPAREN, // (
    RPAREN, // )

    EOF
}