package sample.expressions

import sample.Context
import sample.expressions.unary.UnaryExpression

import java.util.Optional

class ConditionalExpression : UnaryExpression() {
    init {
        priority = 7
    }

    override fun solve(context: Context?): Optional<*> {
        val opt1 = operand1.solve(context)
        return Optional.of(opt1.isPresent && opt1.get() as Boolean)
    }
}
