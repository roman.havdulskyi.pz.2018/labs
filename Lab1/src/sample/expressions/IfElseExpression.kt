package sample.expressions

import sample.Context
import sample.expressions.binary.BinaryExpression
import sample.expressions.binary.IfExpression

import java.util.Optional

class IfElseExpression : BinaryExpression {

    constructor() {
        priority = 9
    }

    constructor(exp1: ExpressionBase, exp2: ExpressionBase?) {
        priority = 9
        this.operand1 = exp1
        this.operand2 = exp2
    }

    override fun solve(context: Context?): Optional<*> {
        if (operand1 is IfExpression) {
            val cond = operand1.solve(context)
            if (cond.isPresent && !(cond.get() as Boolean)) {
                operand2?.solve(context)
            }
        } else {
            operand1.solve(context)
        }
        return Optional.empty<Any>()
    }

}
