package sample.expressions.unary

import sample.Context

import java.util.Optional

class VarExpression : UnaryExpression() {
    init {
        priority = 4
    }

    override fun solve(context: Context?): Optional<*> {
        return this.operand1.solve(context)
    }
}
