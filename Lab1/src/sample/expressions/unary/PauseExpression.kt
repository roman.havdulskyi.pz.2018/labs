package sample.expressions.unary

import sample.Context
import sample.common.Utils
import sample.expressions.ExpressionBase

import java.util.Optional

class PauseExpression(exp: ExpressionBase) : UnaryExpression() {
    init {
        priority = 5
        this.operand1 = exp
    }

    override fun solve(context: Context?): Optional<*> {
        context?.pause(Utils.getIntFromOperand(operand1, context))
        return Optional.empty<Any>()
    }
}
