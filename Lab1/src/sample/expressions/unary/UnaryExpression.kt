package sample.expressions.unary

import sample.expressions.ExpressionBase

abstract class UnaryExpression : ExpressionBase() {
    lateinit var operand1: ExpressionBase
}
