package sample.expressions.unary

import sample.Context
import sample.expressions.ExpressionBase
import sample.expressions.VariableExpression

import java.util.Optional

class PrintExpression : UnaryExpression {

    constructor() : super() {
        priority = 4
    }

    constructor(exp: ExpressionBase) : super() {
        priority = 4
        this.operand1 = exp
    }

    override fun solve(context: Context?): Optional<*> {
        if (this.operand1 is VariableExpression) {
            val optional = this.operand1.solve(context)
            if (optional.isPresent)
                context?.printVar(optional.get() as String)
        } else if (this.operand1 is ConstantExpression) {
            val optional = this.operand1.solve(context)
            if (optional.isPresent)
                context?.printConstant(optional.get() as String)
        }
        return Optional.empty<Any>()
    }
}
