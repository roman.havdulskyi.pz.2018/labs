package sample.expressions.unary

import sample.Context
import sample.common.Utils
import sample.expressions.ExpressionBase

import java.util.Optional

class GoToUrlExpression(exp: ExpressionBase) : UnaryExpression() {

    init {
        priority = 5
        this.operand1 = exp
    }

    override fun solve(context: Context?): Optional<*> {
        context?.let { Utils.getDataFromOperand(operand1, it) }?.let { context.gotoUrl(it) }
        return Optional.empty<Any>()
    }
}
