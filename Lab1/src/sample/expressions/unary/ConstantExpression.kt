package sample.expressions.unary

import sample.Context
import sample.expressions.ExpressionBase

import java.util.Optional

open class ConstantExpression(private val data: String) : ExpressionBase() {

    init {
        priority = 3
    }

    override fun solve(context: Context?): Optional<*> {
        return Optional.of(data)
    }
}
