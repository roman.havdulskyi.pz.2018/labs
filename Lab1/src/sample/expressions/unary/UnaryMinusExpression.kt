package sample.expressions.unary

import sample.expressions.VariableExpression

class UnaryMinusExpression(data: String) : ConstantExpression("-$data")
