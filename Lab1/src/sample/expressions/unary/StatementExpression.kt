package sample.expressions.unary

import sample.Context

import java.util.Optional

class StatementExpression : UnaryExpression() {
    init {
        priority = 7
    }

    override fun solve(context: Context?): Optional<*> {
        return Optional.empty<Any>()
    }
}
