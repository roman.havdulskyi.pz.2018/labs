package sample.expressions.binary

import sample.Context
import sample.expressions.ExpressionBase

import java.util.Optional

class AndExpression(exp1: ExpressionBase, exp2: ExpressionBase) : BinaryExpression() {

    init {
        priority = 5
        this.operand1 = exp1
        this.operand2 = exp2
    }


    override fun solve(context: Context?): Optional<*> {
        var res1 = false
        var res2 = false
        val opt1 = operand1.solve(context)
        var opt2: Optional<*> = Optional.empty<Any>()
        if (operand2 != null)
            opt2 = operand2!!.solve(context)
        if (opt1.isPresent)
            res1 = opt1.get() as Boolean
        if (opt2.isPresent)
            res2 = opt2.get() as Boolean
        return Optional.of(res1 && res2)
    }
}
