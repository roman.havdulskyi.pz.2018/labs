package sample.expressions.binary

import sample.Context
import sample.expressions.ExpressionBase

import java.util.Optional

class WhileExpression : BinaryExpression {

    constructor() {
        priority = 8
    }

    constructor(exp1: ExpressionBase, exp2: ExpressionBase) {
        priority = 8
        this.operand1 = exp1
        this.operand2 = exp2
    }

    override fun solve(context: Context?): Optional<*> {
        var optional: Optional<*> = operand1.solve(context)
        while (optional.isPresent && optional.get() as Boolean) {
            operand2?.solve(context)
            optional = operand1.solve(context)
        }
        return Optional.empty<Any>()
    }
}
