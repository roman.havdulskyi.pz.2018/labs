package sample.expressions.binary.math

import sample.expressions.ExpressionBase
import sample.expressions.binary.BinaryExpression

abstract class MathOperator : BinaryExpression {
    protected constructor() : super() {
        priority = 5
    }

    constructor(exp1: ExpressionBase, exp2: ExpressionBase) : super() {
        priority = 5
        this.operand1 = exp1
        this.operand2 = exp2
    }
}
