package sample.expressions.binary.math

import sample.Context
import sample.common.Utils
import sample.expressions.ExpressionBase

import java.util.Optional

class MinusExpression(exp1: ExpressionBase, exp2: ExpressionBase) : MathOperator(exp1, exp2) {

    override fun solve(context: Context?): Optional<*> {
        return context?.let { Utils.mathWithOperand(operand1, operand2!!, it, MathOperations.Minus) }!!

    }
}
