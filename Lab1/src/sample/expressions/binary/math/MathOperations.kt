package sample.expressions.binary.math

enum class MathOperations {
    Division, Multiply, Minus, Plus
}
