package sample.expressions.binary

import sample.Context
import sample.expressions.ExpressionBase

import java.util.Optional

class AssignmentExpression : BinaryExpression {

    constructor() : super() {
        priority = 6
    }

    constructor(exp1: ExpressionBase, exp2: ExpressionBase) : super() {
        priority = 6
        this.operand1 = exp1
        this.operand2 = exp2
    }

    override fun solve(context: Context?): Optional<*> {
        val opt1 = this.operand1.solve(context)
        val opt2 = this.operand2!!.solve(context)
        if (opt1.isPresent && opt2.isPresent)
            context?.assignVariable(opt1.get() as String, opt2.get())
        return Optional.empty<Any>()
    }
}
