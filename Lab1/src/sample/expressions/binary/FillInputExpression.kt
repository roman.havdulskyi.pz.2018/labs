package sample.expressions.binary

import sample.Context
import sample.common.Utils
import sample.expressions.ExpressionBase

import java.util.Optional

class FillInputExpression(exp: ExpressionBase, exp2: ExpressionBase) : BinaryExpression() {

    init {
        priority = 5
        this.operand1 = exp
        this.operand2 = exp2
    }

    override fun solve(context: Context?): Optional<*> {
        context?.fillInputById(Utils.getDataFromOperand(operand1, context), Utils.getDataFromOperand(operand2!!, context))
        return Optional.empty<Any>()
    }
}
