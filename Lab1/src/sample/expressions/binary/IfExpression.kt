package sample.expressions.binary

import sample.Context
import sample.expressions.ExpressionBase

import java.util.Optional

class IfExpression : BinaryExpression {
    constructor() {
        priority = 8
    }

    constructor(exp1: ExpressionBase, exp2: ExpressionBase) {
        priority = 8
        this.operand1 = exp1
        this.operand2 = exp2
    }

    override fun solve(context: Context?): Optional<*> {
        val cond = operand1.solve(context)
        var result = false
        if (cond.isPresent && cond.get() as Boolean) { //cond is true, then execute Statements
            operand2?.solve(context)
            result = true
        }
        return Optional.of(result)
    }
}

