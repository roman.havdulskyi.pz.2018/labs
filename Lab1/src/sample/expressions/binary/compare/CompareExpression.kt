package sample.expressions.binary.compare

import sample.expressions.binary.BinaryExpression

abstract class CompareExpression : BinaryExpression()
