package sample.expressions.binary.compare

import sample.Context
import sample.common.Utils
import sample.expressions.ExpressionBase

import java.util.Optional

class LessEqualsExpression : CompareExpression {

    constructor() {
        priority = 5
    }

    constructor(exp1: ExpressionBase, exp2: ExpressionBase) {
        priority = 5
        this.operand1 = exp1
        this.operand2 = exp2
    }

    override fun solve(context: Context?): Optional<*> {
        val preResult = context?.let { Utils.compareAsIntOperand(operand1, operand2!!, it) }
        val result: Optional<Boolean>
        result = if (preResult?.isPresent!! && preResult.get() <= 0)
            Optional.of(true)
        else
            Optional.of(false)

        return result
    }
}
