package sample.expressions.binary

import sample.expressions.ExpressionBase
import sample.expressions.unary.UnaryExpression

abstract class BinaryExpression protected constructor() : UnaryExpression() {
    var operand2: ExpressionBase? = null
}
