package sample.expressions

import sample.Context

import java.util.Optional

class FalseExpression : LogicExpression() {
    override fun solve(context: Context?): Optional<*> {
        return Optional.of(false)
    }
}
