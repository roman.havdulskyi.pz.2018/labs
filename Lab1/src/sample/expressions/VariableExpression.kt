package sample.expressions

import sample.Context

import java.util.Optional

class VariableExpression(private val data: String) : ExpressionBase() {

    init {
        priority = 3
    }

    override fun solve(context: Context?): Optional<*> {
        return Optional.of(data)
    }
}
