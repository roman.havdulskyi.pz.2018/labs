package sample.expressions

import sample.Context

import java.util.Optional

class TrueExpression : LogicExpression() {
    override fun solve(context: Context?): Optional<*> {
        return Optional.of(true)
    }
}
