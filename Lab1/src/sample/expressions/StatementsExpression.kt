package sample.expressions

import sample.Context

import java.util.ArrayList
import java.util.Optional

class StatementsExpression : ExpressionBase() {
    val statementsExpressions: MutableList<ExpressionBase>?

    init {
        priority = 7
        statementsExpressions = ArrayList()
    }

    fun addStatement(statementExpression: ExpressionBase?) {
        if (statementExpression == null)
            return
        statementsExpressions!!.add(statementExpression)
    }

    fun addStatements(statementExpression: List<ExpressionBase>?) {
        if (statementExpression == null)
            return
        statementsExpressions!!.addAll(statementExpression)
    }

    fun removeStatements() {
        if (statementsExpressions == null)
            return
        statementsExpressions.clear()
    }

    fun statementsSize(): Int {
        return statementsExpressions!!.size
    }

    override fun solve(context: Context?): Optional<*> {
        if (!statementsExpressions!!.isEmpty()) {
            for (statementExpression in statementsExpressions) {
                statementExpression.solve(context)
            }
        }
        return Optional.empty<Any>()
    }
}
