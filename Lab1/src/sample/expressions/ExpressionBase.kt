package sample.expressions

import sample.Context

import java.util.Optional

abstract class ExpressionBase {

    var priority: Int = 0

    abstract fun solve(context: Context?): Optional<*>
}
