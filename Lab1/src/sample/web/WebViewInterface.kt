package sample.web

interface WebViewInterface {
    fun gotoUrl(url: String)
    fun clickOnButtonById(id: String)
    fun clickOnButtonByTag(id: String)
    fun fillInputById(id: String, text: String)
    fun clickByClassName(name: String)
}
