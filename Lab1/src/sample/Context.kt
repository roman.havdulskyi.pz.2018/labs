package sample

import sample.web.WebViewInterface

import java.util.HashMap
import java.util.Optional

class Context(private val webViewInterface: WebViewInterface, private val iPrintable: IPrintable) {
    private val fakeMemory: MutableMap<String, Any?>

    init {
        this.fakeMemory = HashMap()
    }

    fun addVariable(varName: String) {
        fakeMemory[varName] = null
    }

    fun assignVariable(varName: String, value: Any) {
        fakeMemory[varName] = value
    }

    fun getVariable(varName: String): Any? {
        return fakeMemory[varName]
    }

    fun printConstant(constant: String) {
        iPrintable.println(constant)
    }

    fun printVar(varName: String) {
        val obj = getVariable(varName)
        if (obj == null)
            iPrintable.println("undefined")
        else
            iPrintable.println(obj.toString())
    }

    fun gotoUrl(url: String) {
        webViewInterface.gotoUrl(url)
    }

    fun clickOnBtnById(id: String) {
        webViewInterface.clickOnButtonById(id)
    }

    fun clickOnBtnByTag(tag: String) {
        webViewInterface.clickOnButtonByTag(tag)
    }

    fun fillInputById(id: String, text: String) {
        webViewInterface.fillInputById(id, text)
    }

    fun clickByClassName(name: String) {
        webViewInterface.clickByClassName(name)
    }

    fun pause(ms: Int) {
        try {
            Thread.sleep(ms.toLong())
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    }
}
