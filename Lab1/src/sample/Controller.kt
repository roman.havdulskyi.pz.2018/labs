package sample

import javafx.application.Platform
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.concurrent.Worker
import javafx.fxml.FXML
import javafx.scene.control.TextArea
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import javafx.scene.web.WebEngine
import javafx.scene.web.WebView
import org.w3c.dom.html.HTMLInputElement
import sample.common.Fault
import sample.common.TaskCallback
import sample.common.Utils
import sample.expressions.ExpressionBase
import sample.web.WebViewInterface
import java.util.PriorityQueue
import java.util.Queue

class Controller : WebViewInterface, IPrintable {

    private var jsCommandQueue: Queue<String>? = null
    private var isCompleted = false
    @FXML
    internal var codeInput: TextArea? = null

    @FXML
    internal var webView: WebView? = null

    @FXML
    internal var logView: TextArea? = null

    @FXML
    internal var treeView: TreeView<String>? = null

    @FXML
    internal var treeView1: TreeView<String>? = null

    fun execute() {
        logView!!.clear()
        jsCommandQueue = PriorityQueue()
        val context = Context(this, this)
        webView!!.engine.isJavaScriptEnabled = true
        val code = codeInput!!.text
        try {
            InterpreterV2.ExecuteAsync(code, context, object : TaskCallback<ArrayList<ExpressionBase?>?> {
                override fun onCompleted(result:ArrayList<ExpressionBase?>? , fault: Fault?) {
                    if (result != null && fault == null) {
                        printTree(Utils.genTree(null, result[0]))
                        printTreeOptimized(Utils.genTree(null, result[1]))
                    } else {
                        println(fault!!.exception.message)
                    }
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun printTree(rootItem: TreeItem<String>) {
        Platform.runLater { treeView!!.setRoot(rootItem) }
    }

    private fun printTreeOptimized(rootItem: TreeItem<String>) {
        Platform.runLater { treeView1!!.setRoot(rootItem) }
    }

    private fun waitForLoad() {
        isCompleted = false
        Platform.runLater {
            val webEngine = webView!!.engine
            webEngine.loadWorker.stateProperty().addListener(
                    object : ChangeListener<Worker.State> {
                        override fun changed(
                                observable: ObservableValue<out Worker.State>,
                                oldValue: Worker.State, newValue: Worker.State) {

                            if (newValue == Worker.State.SUCCEEDED) {
                                if (!jsCommandQueue!!.isEmpty()) {
                                    for (jsCommand in jsCommandQueue!!)
                                        webEngine.executeScript(jsCommand)
                                }
                                jsCommandQueue!!.clear()
                                webEngine.loadWorker.stateProperty().removeListener(this)
                                isCompleted = true
                            }

                        }
                    })
        }
    }

    override fun gotoUrl(url: String) {
        Platform.runLater {
            val webEngine = webView!!.engine
            waitForLoad()
            webEngine.load(url)
        }

    }

    override fun clickOnButtonById(id: String) {
        Platform.runLater {
            val command = "var list = document.getElementById(\"" + id + "\");\n" +
                    "list.click()"
            if (isCompleted)
                webView!!.engine.executeScript(command)
            else
                jsCommandQueue!!.add(command)
        }
    }

    override fun clickOnButtonByTag(tag: String) {
        Platform.runLater {
            val element = webView!!.engine.document.getElementsByTagName("input").item(0) as HTMLInputElement
            element.click()
        }
    }

    override fun fillInputById(id: String, text: String) {
        Platform.runLater {
            if (isCompleted)
                webView!!.engine.executeScript("document.getElementById('$id').value='$text';")
            else
                jsCommandQueue!!.add("document.getElementById('$id').value='$text';")
        }

    }

    override fun clickByClassName(name: String) {
        Platform.runLater {
            if (isCompleted)
                webView!!.engine.executeScript("var list = document.getElementsByClassName(\"" + name + "\");\n" +
                        "for (let item of list) {\n" +
                        "    item.click();\n" +
                        "}")
            else
                jsCommandQueue!!.add("var list = document.getElementsByClassName(\"" + name + "\");\n" +
                        "for (let item of list) {\n" +
                        "    item.click();\n" +
                        "}")
        }

    }

    override fun print(text: String) {
        logView!!.text = logView!!.text + text
    }

    override fun println(text: String) {
        logView!!.text = logView!!.text + "\n" + text
    }
}

