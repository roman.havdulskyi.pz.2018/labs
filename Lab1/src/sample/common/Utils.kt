package sample.common

import javafx.scene.control.TreeItem
import sample.Context
import sample.expressions.*
import sample.expressions.binary.*
import sample.expressions.binary.compare.CompareExpression
import sample.expressions.binary.math.MathOperations
import sample.expressions.binary.math.MathOperator
import sample.expressions.unary.ConstantExpression
import sample.expressions.unary.UnaryExpression
import java.util.*

object Utils {

    fun isConstExp(token: String): Boolean {
        return token.contains("\"") && token.length > 2
    }

    fun isNumeric(strNum: String?): Boolean {
        if (strNum == null) {
            return false
        }
        try {
            val d = java.lang.Double.parseDouble(strNum)
        } catch (nfe: NumberFormatException) {
            return false
        }

        return true
    }

    fun isLetterOrDigit(s: String?): Boolean {
        if (s == null)
        // checks if the String is null
            return false
        val len = s.length
        for (i in 0 until len) {
            // checks whether the character is neither a letter nor a digit
            // if it is neither a letter nor a digit then it will return false
            if (!Character.isLetterOrDigit(s[i])) {
                return false
            }
        }
        return true
    }

    fun isNullOrWhitespace(s: String?): Boolean {
        return s == null || isWhitespace(s)

    }

    fun isWhitespace(s: String): Boolean {
        val length = s.length
        if (length > 0) {
            for (i in 0 until length) {
                if (!Character.isWhitespace(s[i])) {
                    return false
                }
            }
            return true
        }
        return false
    }

    fun compareOperand(Operand1: ExpressionBase, Operand2: ExpressionBase, context: Context): Optional<Int> {
        val result: Optional<Int>
        val data1 = getDataFromOperand(Operand1, context)
        val data2 = getDataFromOperand(Operand2, context)
        result = Optional.of(data1.compareTo(data2))
        return result
    }

    fun compareAsIntOperand(Operand1: ExpressionBase, Operand2: ExpressionBase, context: Context): Optional<Int> {
        val result: Optional<Int>
        val data1 = getIntFromOperand(Operand1, context)
        val data2 = getIntFromOperand(Operand2, context)
        result = Optional.of(data1.compareTo(data2))
        return result
    }


    fun mathWithOperand(Operand1: ExpressionBase, Operand2: ExpressionBase, context: Context, mathOperations: MathOperations): Optional<Int> {
        var result = Optional.empty<Int>()
        val data1 = getIntFromOperand(Operand1, context)
        val data2 = getIntFromOperand(Operand2, context)


        if (mathOperations == MathOperations.Plus)
            result = Optional.of(data1 + data2)
        else if (mathOperations == MathOperations.Minus)
            result = Optional.of(data1 - data2)
        else if (mathOperations == MathOperations.Multiply)
            result = Optional.of(data1 * data2)
        else if (mathOperations == MathOperations.Division)
            result = Optional.of(data1 / data2)

        return result
    }


    fun getIntFromOperand(expressionBase: ExpressionBase, context: Context): Int {
        if (expressionBase is ConstantExpression) {
            val optionalS = expressionBase.solve(context)
            if (optionalS.isPresent)
                return Integer.parseInt(optionalS.get().toString())

        } else if (expressionBase is VariableExpression) {
            val optionalS = expressionBase.solve(context)
            if (optionalS.isPresent)
                return Integer.parseInt(context.getVariable(optionalS.get().toString()).toString())
        } else if (expressionBase is MathOperator) {
            val optionalS = expressionBase.solve(context)
            if (optionalS.isPresent)
                return Integer.parseInt(optionalS.get().toString())
        }
        throw IllegalStateException()
    }

    fun getDataFromOperand(expressionBase: ExpressionBase, context: Context): String {
        if (expressionBase is ConstantExpression) {
            val optionalS = expressionBase.solve(context)
            if (optionalS.isPresent)
                return optionalS.get().toString()

        } else if (expressionBase is VariableExpression) {
            val optionalS = expressionBase.solve(context)
            if (optionalS.isPresent)
                return context.getVariable(optionalS.get().toString()).toString()
        } else if (expressionBase is MathOperator) {
            val optionalS = expressionBase.solve(context)
            if (optionalS.isPresent)
                return optionalS.get().toString()
        }
        throw IllegalStateException()
    }

    fun genTree(base: TreeItem<String>?, exp: ExpressionBase?): TreeItem<String> {
        var base = base
        if (base == null) {
            base = TreeItem()
            base.isExpanded = true
        }
        if (exp != null) {
            val treeItem = TreeItem(exp.toString())
            base.children.add(treeItem)
            if (exp is StatementsExpression) {
                val statementsExpression = exp as StatementsExpression?
                if (statementsExpression!!.statementsSize() > 0)
                    for (expressionBase in statementsExpression.statementsExpressions!!)
                        genTree(treeItem, expressionBase)
            } else if (exp is BinaryExpression) {
                genTree(treeItem, exp.operand1)
                genTree(treeItem, exp.operand2)
            } else if (exp is UnaryExpression) {
                genTree(treeItem, exp.operand1)
            }
        }

        return base

    }

fun optimizeConditional(exp : ExpressionBase?, context: Context) : ExpressionBase?
    {
        var expression = exp
        if(exp is CompareExpression)
        {
            if(exp.operand1 is ConstantExpression && exp.operand2 is ConstantExpression)
            {
                val res = exp.solve(context)
                if(res.isPresent) {
                    if (res.get() == true)
                        return TrueExpression()
                    else
                        return FalseExpression()
                }
            } else {
                return exp;
            }
        } else if(exp is BinaryExpression)
        {
            val exp1  = optimizeConditional(exp.operand1, context)
            val exp2 = optimizeConditional(exp.operand2, context)
            if (exp1 is LogicExpression || exp2 is LogicExpression ) {
                if (exp1 is LogicExpression && exp2 is LogicExpression) {
                    return if(exp is OrExpression) {
                        if (exp1 is TrueExpression || exp2 is TrueExpression)
                            TrueExpression()
                        else
                            FalseExpression()
                    } else {
                        if (exp1 is TrueExpression && exp2 is TrueExpression)
                            TrueExpression()
                        else
                            FalseExpression()
                    }
                } else if (exp1 is LogicExpression) {
                    return if(exp is OrExpression) {
                        if(exp1 is TrueExpression)
                            TrueExpression()
                        else
                            exp2
                    } else {
                        if(exp1 is FalseExpression)
                            FalseExpression()
                        else
                            exp2
                    }
                } else {
                    return if(exp is OrExpression) {
                        if(exp2 is TrueExpression)
                            TrueExpression()
                        else
                            exp1
                    } else {
                        if(exp2 is FalseExpression)
                            FalseExpression()
                        else
                            exp1
                    }
                }
            }

        }

        return exp
    }

    fun optimizeStatementsVariable(exp : ExpressionBase) : ExpressionBase?
    {
        if(exp is StatementsExpression)
        {
            val expList  = mutableListOf<ExpressionBase>()
            val tempList = mutableListOf<ExpressionBase>()
            if(exp.statementsSize() > 0)
                for ((index, item) in exp.statementsExpressions?.withIndex()!!) {
                    if(item is MathOperations?)
                        continue

                    when (item) {
                        is StatementsExpression -> {
                            if(tempList.isNotEmpty())
                                expList.addAll(tempList)
                            tempList.clear()
                            val  temp = optimizeStatementsVariable(item)
                            temp?.let { expList.add(it) }
                        }
                        is AssignmentExpression? -> tempList.add(item)
                        else -> {
                            if(tempList.isNotEmpty())
                                expList.addAll(tempList)
                            tempList.clear()
                            expList.add(item)
                        }
                    }
                }
        }
        return exp
    }

//    private fun optimizeAssignments(list : MutableList<ExpressionBase>) : MutableList<ExpressionBase>
//    {
//        if(list.size == 1)
//            return list
//        val temp = mutableListOf<ExpressionBase>()
//        Collections.copy(temp, list)
//        temp.dropLast(1)
//        list.reverse()
//        val tempExp = list.last()
//        val result = mutableListOf<ExpressionBase>()
//        for(item in temp)
//        {
//            if(tempExp is BinaryExpression) {
//                if (item is BinaryExpression) {
//                    if (item.operand1 is VariableExpression) {
//                        if (item.operand1.solve(null).get().toString() == tempExp.operand1.)
//                    } else {
//
//                    }
//                }
//            }
//        }
////        for ((index, item) in list.withIndex()) {
////           if(index == list.size) {
////
////           } else {
////
////           }
////        }
//    }
}
