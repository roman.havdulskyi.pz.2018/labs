package sample.common

@FunctionalInterface
interface TaskCallback<T> {
    fun onCompleted(result: T, fault: Fault?)
}