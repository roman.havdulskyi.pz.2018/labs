package sample.common

class Fault {
    val exception: Throwable

    constructor(exception: Throwable) {
        this.exception = exception
    }

    constructor(errorMessage: String) {
        this.exception = Throwable(errorMessage)
    }
}
