package sample

import sample.expressions.*
import sample.expressions.binary.*
import sample.expressions.binary.compare.*
import sample.expressions.binary.math.DivisionExpression
import sample.expressions.binary.math.MinusExpression
import sample.expressions.binary.math.MultiplyExpression
import sample.expressions.binary.math.PlusExpression
import sample.expressions.unary.*

class Parser(private val tokens: List<Token>) {
    private val size: Int = tokens.size

    private var pos: Int = 0

    fun parse(): ExpressionBase {
        val result = StatementsExpression()
        while (!match(TokenType.EOF)) {
            result.addStatement(statement())
        }
        return result
    }

    private fun block(): ExpressionBase {
        val block = StatementsExpression()
        consume(TokenType.LBRACE)
        while (!match(TokenType.RBRACE)) {
            block.addStatement(statement())
        }
        return block
    }

    private fun statementOrBlock(): ExpressionBase? {
        return if (get().type == TokenType.LBRACE) block() else statement()
    }

    private fun statement(): ExpressionBase? {
        if (match(TokenType.GOTOURL))
            return expression()?.let { GoToUrlExpression(it) }
        if (match(TokenType.PAUSE))
            return expression()?.let { PauseExpression(it) }
        if (match(TokenType.CLICKBYCLASS)) {
            return expression()?.let { ClickByClassNameExpression(it) }
        }
        if (match(TokenType.CLICKBYID)) {
            return expression()?.let { ClickBtnByIdExpression(it) }
        }
        if (match(TokenType.FILLINPUT)) {
            return expression()?.let { expression()?.let { it1 -> FillInputExpression(it, it1) } }
        }
        if (match(TokenType.PRINT)) {
            return expression()?.let { PrintExpression(it) }
        }
        if (match(TokenType.IF)) {
            return ifElse()
        }
        return if (match(TokenType.WHILE)) {
            whileStatement()
        } else assignmentStatement()

    }

    private fun assignmentStatement(): ExpressionBase? {
        // WORD EQ
        val current = get()
        if (match(TokenType.WORD) && get().type == TokenType.EQ) {
            val variable = current.text
            consume(TokenType.EQ)
            return variable?.let { VariableExpression(it) }?.let { expression()?.let { it1 -> AssignmentExpression(it, it1) } }
        }
        throw RuntimeException("Unknown statement")
    }

    private fun ifElse(): ExpressionBase? {
        val condition = expression()
        val ifStatement = statementOrBlock()
        val elseStatement: ExpressionBase?
        if (match(TokenType.ELSE)) {
            elseStatement = statementOrBlock()
        } else {
            elseStatement = null
        }

        return if(elseStatement == null)
        {
            IfElseExpression(IfExpression(condition!!, ifStatement!!), null)
        } else {
            IfElseExpression(IfExpression(condition!!, ifStatement!!), elseStatement)
        }
    }

    private fun whileStatement(): ExpressionBase? {
        val condition = expression()
        val statement = statementOrBlock()
        return statement?.let { condition?.let { it1 -> WhileExpression(it1, it) } }
    }

    private fun expression(): ExpressionBase? {
        return logicalOr()
    }

    private fun logicalOr(): ExpressionBase? {
        var result = logicalAnd()

        while (true) {
            if (match(TokenType.BARBAR)) {
                result = logicalAnd()?.let { result?.let { it1 -> OrExpression(it1, it) } }
                continue
            }
            break
        }

        return result
    }

    private fun logicalAnd(): ExpressionBase? {
        var result = equality()

        while (true) {
            if (match(TokenType.AMPAMP)) {
                result = result?.let { equality()?.let { it1 -> AndExpression(it, it1) } }
                continue
            }
            break
        }

        return result
    }

    private fun equality(): ExpressionBase? {
        val result = conditional()

        if (match(TokenType.EQEQ)) {
            return conditional()?.let { result?.let { it1 -> EqualsExpression(it1, it) } }
        }
        return if (match(TokenType.EXCLEQ)) {
            result?.let { conditional()?.let { it1 -> NotEqualsExpression(it, it1) } }
        } else result

    }

    private fun conditional(): ExpressionBase? {
        var result = additive()

        while (true) {
            if (match(TokenType.LT)) {
                result = result?.let { additive()?.let { it1 -> LessExpression(it, it1) } }
                continue
            }
            if (match(TokenType.LTEQ)) {
                result = additive()?.let { result?.let { it1 -> LessEqualsExpression(it1, it) } }
                continue
            }
            if (match(TokenType.GT)) {
                result = result?.let { additive()?.let { it1 -> GreatExpression(it, it1) } }
                continue
            }
            if (match(TokenType.GTEQ)) {
                result = additive()?.let { result?.let { it1 -> GreatEqualsExpression(it1, it) } }
                continue
            }
            break
        }

        return result
    }

    private fun additive(): ExpressionBase? {
        var result = multiplicative()

        while (true) {
            if (match(TokenType.PLUS)) {
                result = result?.let { multiplicative()?.let { it1 -> PlusExpression(it, it1) } }
                continue
            }
            if (match(TokenType.MINUS)) {
                result = multiplicative()?.let { result?.let { it1 -> MinusExpression(it1, it) } }
                continue
            }
            break
        }

        return result
    }

    private fun multiplicative(): ExpressionBase? {
        var result = unary()

        while (true) {
            // 2 * 6 / 3
            if (match(TokenType.STAR)) {
                result = result?.let { unary()?.let { it1 -> MultiplyExpression(it, it1) } }
                continue
            }
            if (match(TokenType.SLASH)) {
                result = unary()?.let { result?.let { it1 -> DivisionExpression(it1, it) } }
                continue
            }
            break
        }

        return result
    }

    private fun unary(): ExpressionBase? {
        if (match(TokenType.MINUS)) {
            val exp = primary()
            if (exp is ConstantExpression) {
                val opt = exp.solve(null)
                if (opt.isPresent)
                    return UnaryMinusExpression(opt.get().toString())
            }
        }
        return if (match(TokenType.PLUS)) {
            primary()
        } else primary()
    }

    private fun primary(): ExpressionBase? {
        val current = get()
        if (match(TokenType.NUMBER)) {
            return current.text?.let { ConstantExpression(it) }
        }
        if (match(TokenType.HEX_NUMBER)) {
            return ConstantExpression(java.lang.Long.parseLong(current.text, 16).toString())
        }
        if (match(TokenType.WORD)) {
            return current.text?.let { VariableExpression(it) }
        }
        if (match(TokenType.TEXT)) {
            return current.text?.let { ConstantExpression(it) }
        }
        if (match(TokenType.LPAREN)) {
            val result = expression()
            match(TokenType.RPAREN)
            return result
        }
        throw RuntimeException("Unknown expression")
    }

    private fun consume(type: TokenType) {
        val current = get()
        if (type != current.type) throw RuntimeException("Token $current doesn't match $type")
        pos++
    }

    private fun match(type: TokenType): Boolean {
        val current = get()
        if (type != current.type) return false
        pos++
        return true
    }

    private fun get(): Token {
        val position = pos
        return if (position >= size) EOF else tokens[position]
    }

    companion object {

        private val EOF = Token(TokenType.EOF, "")
    }
}